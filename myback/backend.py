from flask import Flask, session, request, redirect

app = Flask(__name__)

app.secret_key = 'any random string'

@app.route('/data')
def show_data():
    fn = session['_fn']
    ln = session['_ln']
    return { 'Fname': fn, 'Lname': ln }

@app.route('/loginprocess', methods = ['POST'])
def loginprocfunc():
    session.clear()
    if request.method == 'POST':
        session['_fn'] = request.form['fn']
        session['_ln'] = request.form['ln']
        return redirect("http://127.0.0.1:3000")

if __name__  == '__main__':

    app.run(debug=True, host='0.0.0.0', port=5000 )
