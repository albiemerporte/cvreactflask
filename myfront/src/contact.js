import React from "react";

function Mycontact(){

    return(
        <>
			<h1>CONTACT</h1>
                <p>If you want to work with me, you can contact me via various forms: </p><br />
                
                <p><i class="material-icons" >local_phone</i>
                09771348160 | (02)83627681</p>
                
                <p><a class='materiallink' href = "mailto:albiemerporte@gmail.com"><i class="material-icons" >email</i>albiemerporte@gmail.com</a></p>
                
                <p><a class='githublink' href = "https://github.com/albiemer"><i class="fa fa-github" > albiemer</i></a></p>
                
                <p><a class='linkedinlink' href = "https://www.linkedin.com/in/albiemer-porte/"><i class="fa fa-linkedin-square" > www.linkedin.com/in/albiemer-porte</i></a></p>
		
        </>
    );

}

export default Mycontact;