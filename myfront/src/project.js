
import React from 'react';


function Myproject(){
    return (
        <>
                    <h1>PERSONAL PROJECT</h1>
            
                <table class='maintbl' >

                    <thead>
                        <tr>
                        <th>Software</th>
                        <th>Language</th>
                        <th>Type Of Project</th>
                        <th>Link</th>
                        </tr>
                    </thead>
                        <tr>
                            <td>portfoliosite-php-version-discontinued</td>
                            <td>PHP, HTML, CSS, JS</td>
                            <td>Dynamic Website (Non Framework)</td>
                            <td><a href = "https://github.com/albiemer/portfoliosite-php-version-discontinued">Github</a></td>
                        </tr>{ /* <!--End Row--> */ }
                        <tr>
                            <td>Bitcoin book</td>
                            <td>Python3</td>
                            <td>Terminal Application</td>
                            <td><a href = "https://github.com/albiemer/Bitcoinbook">Github</a></td>
                        </tr>{ /* <!--END ROW--> */ }
                        <tr>
                            <td>Library-Management-System-Pure-Python3</td>
                            <td>Python3</td>
                            <td>Graphical User Interface Application</td>
                            <td><a href = "https://github.com/albiemer/Library-Management-System-Pure-Python3">Github</a></td>
                        </tr>{ /* <!--END ROW--> */ }
                        <tr>
                            <td>Hybrid-Password-Manager </td>
                            <td>Python3, HTML, CSS, JS, Flask</td>
                            <td>Hybrid Application</td>
                            <td><a href = "https://github.com/albiemer/Hybrid-Password-Manager">Github</a></td>
                        </tr>{ /* <!--END ROW--> */ }
                        <tr>
                            <td>BankDriv</td>
                            <td>Python3</td>
                            <td>Terminal Application</td>
                            <td><a href = "https://github.com/albiemer/bankdrive">Github</a></td>
                        </tr>{ /* <!--END ROW--> */ }
                        <tr>
                            <td>Bingo Auto</td>
                            <td>Python3</td>
                            <td>Terminal Application</td>
                            <td><a href = "https://github.com/albiemer/Bingoauto">Github</a></td>
                        </tr> { /* <!--END ROW--> */ }
                    </table><br />
                    
                    <hr />
                    
                    <h1>ACADEMIC PROJECT</h1>
            
                <table class='maintbl'>

                    <thead>
                        <tr>
                        <th>Software</th>
                        <th>Language</th>
                        <th>Type Of Project</th>
                        <th>Link</th>
                        </tr>
                    </thead>
                        <tr>
                            <td>OLWS1-Activity2</td>
                            <td>HTML</td>
                            <td>Simple Website</td>
                            <td><a href = "https://github.com/albiemer/OLWS1/blob/main/Activity2.html">Github</a></td>
                        </tr> { /* <!--End Row--> */ }
                        <tr>
                            <td>OLWS1-Activity3</td>
                            <td>HTML</td>
                            <td>Simple Lyrics Websites</td>
                            <td><a href = "https://github.com/albiemer/Bitcoinbook">Github</a></td>
                        </tr> { /* <!--End Row--> */ }
                        <tr>
                            <td>OLWS1-Activity4-Phrase</td>
                            <td>HTML</td>
                            <td>Phrase format</td>
                            <td><a href = "https://github.com/albiemer/OLWS1/blob/main/Phrase.html">Github</a></td>
                        </tr> { /* <!--END ROW--> */}
                        <tr>
                            <td>OLWS1-Activity4-Formatting</td>
                            <td>HTML</td>
                            <td>HTML Formatting</td>
                            <td><a href = "https://github.com/albiemer/OLWS1/blob/main/Formatting.html">Github</a></td>
                        </tr> { /* <!--END ROW--> */ }
                        <tr>
                            <td>OLWS1-Activity5-6</td>
                            <td>HTML</td>
                            <td>Simple Web</td>
                            <td><a href = "https://github.com/albiemer/OLWS1/blob/main/Activity5-6.html">Github</a></td>
                        </tr> { /* <!--END ROW--> */ }
                        <tr>
                            <td>16 BASIC COLOR NAMES IN HTML</td>
                            <td>HTML</td>
                            <td>HTML Table</td>
                            <td><a href = "https://github.com/albiemer/OLWS1/blob/main/ACTIVITY7.html">Github</a></td>
                        </tr>{ /* <!--END ROW--> */ }
                        <tr>
                            <td>16 BASIC COLOR NAMES IN HTML</td>
                            <td>HTML</td>
                            <td>HTML Table</td>
                            <td><a href = "https://github.com/albiemer/OLWS1/blob/main/ACTIVITY7.html">Github</a></td>
                        </tr>{ /* <!--END ROW--> */ }
                    </table>
                    
        </>
        
    );
}

export default Myproject;