import React from 'react';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';
import Myhome from './home';
import Mycontact from './contact';
import Myproject from './project';
import Tologin from './login';



function App(){
  return (
    <>
      <BrowserRouter>
        <div class = "headofpage">
          <Link to='/home'><i class="fa fa-home">HOME</i></Link> | 
          <Link to='/contact'>CONTACT</Link> |
          <Link to='/project'>PROJECT</Link> | 
          <Link to='/login'>LOGIN</Link>
        </div>
        <br />

        <div class = "content">
          <Routes>
            <Route path='/' element={<Myhome />} />
            <Route path='/home' element={<Myhome />} />
            <Route path='/contact' element={<Mycontact />} />
            <Route path='/project' element={<Myproject />} />
            <Route path='/login' element={<Tologin />} />
          </Routes>
        </div><br />
        
        <div class = "footer">
          <h4>Copyrighted 2022</h4>
        </div>
      </BrowserRouter>
                      
    </>
  );
}

export default App;
